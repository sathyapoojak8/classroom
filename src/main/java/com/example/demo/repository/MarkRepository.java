package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Subject;
import com.example.demo.model.subjectMark;

public interface MarkRepository extends CrudRepository<subjectMark,Long> {
    List<subjectMark> findBySubject(Subject teacherSubject);
}
