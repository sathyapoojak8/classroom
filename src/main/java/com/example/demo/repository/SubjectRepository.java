package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Subject;

@Repository
public interface SubjectRepository extends CrudRepository<Subject,Long> {
 List<Subject> findAll();
 Subject findBySubjectName(String subjectName);
}
