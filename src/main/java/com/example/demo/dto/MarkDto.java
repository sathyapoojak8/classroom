package com.example.demo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarkDto {
	@NotBlank
	private String subName;
	@NotBlank
	private String studentName;
	@NotNull
	private int mark;




}
