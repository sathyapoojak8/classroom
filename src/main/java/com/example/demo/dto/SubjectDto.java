package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubjectDto {

    private String subjectName;
    private Long subjectId;
    //private List<Student> studentName;

}
