package com.example.demo.dto;

import com.example.demo.model.subjectMark;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentDto {
    private String name;
    private List<subjectMark> subjectMarks;


}
