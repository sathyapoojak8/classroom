package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity(name="Teacher")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Teacher {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @ManyToOne
    private Subject subject;



}
