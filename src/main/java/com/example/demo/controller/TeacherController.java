package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.TeacherDto;
import com.example.demo.service.TeacherService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    TeacherService teacherService;
    @PostMapping("/addName")
    public ResponseEntity<TeacherDto> teacherNames(@RequestBody TeacherDto teacherDto){
        log.info("Entered into Student Controller - teacherNames method");
        TeacherDto teacher = teacherService.addTeacher(teacherDto.getName(),teacherDto.getSubName());
        return new ResponseEntity<>(teacher, HttpStatus.OK);

    }
    @GetMapping("/getSubMarks")
    public ResponseEntity<List<Integer>> teacherSubMarks(@RequestBody TeacherDto teacherDto){
        log.info("Entered into Student Controller - teacherNames method");
        List<Integer> total = teacherService.teacherSubMarks(teacherDto.getName());
        return new ResponseEntity<>(total, HttpStatus.OK);

    }
}
