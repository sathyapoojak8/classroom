package com.example.demo.controller;

import javax.naming.InvalidNameException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.ListMarkDto;
import com.example.demo.dto.MarkDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.service.MarkService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/data")
public class MarkController {

	// injecting bean
	private final MarkService markService;
	MarkController(MarkService markService) {

		this.markService = markService;
	}
	@PostMapping("/addMark")
	public ResponseEntity<String> addMark(@RequestBody MarkDto markDto) throws InvalidNameException {
		log.info("Entered into Mark Controller :: addMark method");
		if (markDto == null || markDto.getSubName() == null) {
			return new ResponseEntity<>("Invalid Request", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(markService.addMark(markDto), HttpStatus.OK);
	}


	@PostMapping("/addMarks")
	public ResponseEntity<String> addMarks(@RequestBody ListMarkDto listMarkDto) throws InvalidNameException {
		log.info("Entered into Mark Controller :: addMarks method");

		for (MarkDto dataAll : listMarkDto.getData()) {
			if (dataAll == null || dataAll.getSubName() == null) {
				return new ResponseEntity<>("Invalid request", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<>(markService.addMarks(listMarkDto.getData()), HttpStatus.OK);
	}

	@GetMapping("/total")
	public ResponseEntity<Integer> getTotal(@RequestBody StudentDto studentDto){
		log.info("Entered into Student Controller - getTotal method");
		int total = markService.marksSummary(studentDto.getName());
		return new ResponseEntity<>(total, HttpStatus.OK);
	}
}
