package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.SubjectDto;
import com.example.demo.service.SubjectService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/studentSubject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @PostMapping("/subjects")
    public ResponseEntity<SubjectDto> addMarks(@RequestBody SubjectDto subjectDto){
        log.info("Entered into subjects Method");
        SubjectDto subject = subjectService.addSubjects(subjectDto.getSubjectName(),subjectDto.getSubjectId());
        return new ResponseEntity<>(subject, HttpStatus.OK);
    }

}


