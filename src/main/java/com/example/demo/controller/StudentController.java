package com.example.demo.controller;

import com.example.demo.dto.StudentDto;
import com.example.demo.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/student")
public class StudentController {
    @Autowired
    StudentService studentService;
    @PostMapping("/add")
    public ResponseEntity<StudentDto> addNames(@RequestBody StudentDto studentDto){
        log.info("Entered into Student Controller - addNames method");
        StudentDto student = studentService.addStudent(studentDto.getName());
        return new ResponseEntity<>(student, HttpStatus.OK);
    }


}
