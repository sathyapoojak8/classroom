package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.TeacherDto;
import com.example.demo.model.Subject;
import com.example.demo.model.Teacher;
import com.example.demo.model.subjectMark;
import com.example.demo.repository.MarkRepository;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.repository.TeacherRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TeacherService {
    private final TeacherRepository teacherRepository;
    private final SubjectRepository subjectRepository;
    private final MarkRepository markRepository;

    TeacherService(TeacherRepository teacherRepository, SubjectRepository subjectRepository, MarkRepository markRepository){
        this.teacherRepository = teacherRepository;
        this.subjectRepository = subjectRepository;
        this.markRepository = markRepository;
    }
    public TeacherDto addTeacher(String name,String subName) {
        log.info("Entered into TeacherService - addTeacher Method");
        TeacherDto teacherDto = new TeacherDto();
        Teacher teacher = new Teacher();
        teacher.setName(name);
        Subject subject = subjectRepository.findBySubjectName(subName);
        teacher.setSubject(subject);
        teacherRepository.save(teacher);
        teacherDto.setName(teacher.getName());
        teacherDto.setSubName(teacher.getSubject().getSubjectName());
        return teacherDto;
    }

    //get the subjects mapped to that particular teacher
    //with help of subject going to get all the marks from subjectMark
    //use counter and get the avg


    public List<Integer> teacherSubMarks(String name) {
        log.info("Entered into TeacherService - teacherSubMarks Method");
        List<Integer> al = new ArrayList<>();
        int sum =0;
        int count = 1;
        Teacher teacherName = teacherRepository.findByName(name);
        Subject teacherSubject = teacherName.getSubject();
        List<subjectMark> allMarks = markRepository.findBySubject(teacherSubject);
            for (subjectMark mark : allMarks) {
                sum+= mark.getMarks();
                count++;
            }
            al.add(sum);
            al.add(sum/(count - 1));
        return al;
    }
}
