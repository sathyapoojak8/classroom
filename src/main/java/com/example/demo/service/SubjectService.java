package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.SubjectDto;
import com.example.demo.model.Subject;
import com.example.demo.repository.SubjectRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    public SubjectDto addSubjects(String subjectName,Long subId){
        log.info("Entered into SubjectService - add subjects Method");
        SubjectDto subjectDto = new SubjectDto();
        Subject subject = new Subject();
        subject.setSubjectName(subjectName);
        subject.setSubjectId(subId);
        subjectRepository.save(subject);
        subjectDto.setSubjectName(subject.getSubjectName());
        subjectDto.setSubjectId(subject.getSubjectId());
        return subjectDto;

    }
}
