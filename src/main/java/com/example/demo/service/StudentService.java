package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.StudentDto;
import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;

	public StudentDto addStudent(String name) {
		log.info("Entered into StudentService - addStudent Method");
		StudentDto studentDto = new StudentDto();
		Student student = new Student();
		student.setName(name);
		studentRepository.save(student);
		studentDto.setName(student.getName());
		return studentDto;
	}


}
