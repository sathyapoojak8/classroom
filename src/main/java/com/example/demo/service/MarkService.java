package com.example.demo.service;

import java.util.List;

import javax.naming.InvalidNameException;

import org.springframework.stereotype.Service;

import com.example.demo.dto.MarkDto;
import com.example.demo.model.Student;
import com.example.demo.model.Subject;
import com.example.demo.model.subjectMark;
import com.example.demo.repository.MarkRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.SubjectRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MarkService {

	private final StudentRepository studentRepository;
	private final MarkRepository markRepository;
	private final SubjectRepository subjectRepository;
	public String res = "SUCCESS";
	MarkService(MarkRepository markRepository, SubjectRepository subjectRepository,
			StudentRepository studentRepository) {
		this.markRepository = markRepository;
		this.subjectRepository = subjectRepository;
		this.studentRepository = studentRepository;

	}

	public String addMark(MarkDto markDto) throws InvalidNameException {

		Student student = studentRepository.findByName(markDto.getStudentName());
		Subject subject = subjectRepository.findBySubjectName(markDto.getSubName());

		if (student == null || subject == null) {
			throw new InvalidNameException();
		}
		subjectMark subjectMark = new subjectMark();
		subjectMark.setMarks(markDto.getMark());
		subjectMark.setSubject(subject);
		markRepository.save(subjectMark);

		List<subjectMark> subjectMarks = student.getSubjectMarks();
		subjectMarks.add(subjectMark);
		studentRepository.save(student);

		return res;
	}

	public String addMarks(List<MarkDto> marksDto) throws InvalidNameException {
		for (MarkDto markDto : marksDto) {
			addMark(markDto);
		}
		return res;
	}

	// get name from input
	// fetch student data from db based on name
	// get all the subjectMarks and iterate
	// add all the marks and return it
	public int marksSummary(String name) {
		Student student = studentRepository.findByName(name);
		int sum = 0;
		for (subjectMark subjectMark : student.getSubjectMarks()) {
			sum += subjectMark.getMarks();
		}
		return sum;
	}
}
